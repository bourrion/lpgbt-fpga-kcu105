##===================================================================================================##
##========================================  I/O PINS  ===============================================##
##===================================================================================================##

##==============##
## FABRIC CLOCK ##
##==============##
set_property IOSTANDARD LVDS [get_ports USER_CLOCK_P]
set_property PACKAGE_PIN G10 [get_ports USER_CLOCK_P]
set_property IOSTANDARD LVDS [get_ports USER_CLOCK_N]
set_property PACKAGE_PIN F10 [get_ports USER_CLOCK_N]

##==========##
## MGT(GTH) ##
##==========##

## MGT CLOCK:
##--------------

## Comment: * The MGT reference clock MUST be provided by an external clock generator.
##
##          * The MGT reference clock frequency must be 320MHz.

set_property PACKAGE_PIN V6 [get_ports SMA_MGT_REFCLK_P]
set_property PACKAGE_PIN V5 [get_ports SMA_MGT_REFCLK_N]

## SERIAL LANES:
##--------------

# Use of SFP
set_property PACKAGE_PIN U4 [get_ports SFP0_TX_P]
set_property PACKAGE_PIN U3 [get_ports SFP0_TX_N]
set_property PACKAGE_PIN T2 [get_ports SFP0_RX_P]
set_property PACKAGE_PIN T1 [get_ports SFP0_RX_N]

## SFP CONTROL:
##-------------
set_property PACKAGE_PIN AL8 [get_ports SFP0_TX_DISABLE]
set_property IOSTANDARD LVCMOS18 [get_ports SFP0_TX_DISABLE]

##====================##
## SIGNALS FORWARDING ##
##====================##

## SMA OUTPUT:
##------------
set_property PACKAGE_PIN H27 [get_ports USER_SMA_GPIO_P]
set_property IOSTANDARD LVCMOS18 [get_ports USER_SMA_GPIO_P]
set_property SLEW FAST [get_ports USER_SMA_GPIO_P]

set_property PACKAGE_PIN G27 [get_ports USER_SMA_GPIO_N]
set_property IOSTANDARD LVCMOS18 [get_ports USER_SMA_GPIO_N]
set_property SLEW FAST [get_ports USER_SMA_GPIO_N]